package com.gitlab.marcinogo.cidemo;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

/**
 * @author Marcin Ogorzałek
 */
public class MainTest {
    @Test
    public void isTrue_shouldReturnFalse_whenFalseAsParam() {
        // Given
        // When
        var actual = Main.isTrue(false);
        // Then
        assertFalse(actual);
    }

    @Test
    public void isTrue_shouldReturnTrue_whenTrueAsParam() {
        // Given
        // When
        var actual = Main.isTrue(true);
        // Then
        assertTrue(actual);
    }
}
